package pl.edu.vistula.annarapacz.restcl;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by arapa on 2017-12-29.
 */

public class Zlecenie
{
    @JsonProperty("id")
    private long id;
    @JsonProperty("info")
    private String info ;
    @JsonProperty("name")
    private String name ;
    @JsonProperty("street")
    private String street ;
    @JsonProperty("street2")
    private String street2 ;
    @JsonProperty("city")
    private String city ;
    @JsonProperty("phone")
    private String phone ;
    @JsonProperty("phone2")
    private String phone2 ;
    @JsonProperty("email")
    private String email ;
    @JsonProperty("terminalTID")
    private String terminalTID ;
    @JsonProperty("terminalSerial")
    private String terminalSerial ;
    @JsonProperty("terminalModel")
    private String terminalModel ;
    @JsonProperty("terminalDescription")
    private String terminalDescription ;
    @JsonProperty("simImei")
    private String simImei ;
    @JsonProperty("simDesc")
    private String simDesc ;
    @JsonProperty("installDate")
    private String installDate ;
    @JsonProperty("status")
    private String status;



    public String ToString()
    {
        String z = "Zlecenie:\nID=" + String.valueOf(id) + "\nPunkt=" + name + "\nUlica=" + street + "\nMiasto=" + city;
        return z;
    }

    public long getId()
    {
        return this.id;
    }
    public String getInfo()
    {
        return this.info;
    }
    public String getName()
    {
        return this.name;
    }
    public String getStreet()
    {
        return this.street;
    }
    public String getStreet2()
    {
        return this.street2;
    }
    public String getCity()
    {
        return this.city;
    }
    public String getPhone()
    {
        return this.phone;
    }
    public String getPhone2()
    {
        return this.phone2;
    }
    public String getEmail()
    {
        return this.email;
    }
    public String getTerminalTID() {return this.terminalTID;}
    public String getTerminalSerial() {return this.terminalSerial;}
    public String getTerminalModel() {return this.terminalModel;}
    public String getTerminalDescription() {return this.terminalDescription;}
    public String getSimImei() {return this.simImei;}
    public String getSimDesc() {return this.simDesc;}
    public String getInstallDate()
    {
        return this.installDate;
    }
    public String getStatus() {return this.status;}



    public void setId(long id)
    {
        this.id = id;
    }
    public void setInfo(String info)
    {
        this.info = info;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setStreet(String street)
    {
        this.street = street;
    }
    public void setStreet2(String street2)
    {
        this.street2 = street2;
    }
    public void setCity(String city)
    {
        this.city = city;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    public void setPhone2(String phone2)
    {
        this.phone2 = phone2;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public void setTerminalTID(String terminalTID) {this.terminalTID = terminalTID;}
    public void setTerminalSerial(String terminalSerial) {this.terminalSerial = terminalSerial;}
    public void setTerminalModel(String terminalModel) {this.terminalModel = terminalModel;}
    public void setTerminalDescription(String terminalDescription) {this.terminalDescription = terminalDescription;}
    public void setSimImei(String simImei) {this.simImei = simImei;}
    public void setSimDesc(String simDesc) {this.simDesc = simDesc;}
    public void setInstallDate(String date) {this.installDate = date;}
    public void setStatus(String status) {this.status = status;}



    public Zlecenie(long id, String info, String name, String street, String street2, String city, String phone, String phone2, String email,
                    String terminalTID, String terminalSerial, String terminalModel, String terminalDescription, String simImei, String simDesc,
                    String installDate, String status)
    {
        this.id = id;
        this.info = info;
        this.name = name;
        this.street = street;
        this.street2 = street2;
        this.city = city;
        this.phone = phone;
        this.phone2 = phone2;
        this.email = email;
        this.terminalTID = terminalTID;
        this.terminalSerial = terminalSerial;
        this.terminalModel = terminalModel;
        this.terminalDescription = terminalDescription;
        this.simImei = simImei;
        this.simDesc = simDesc;
        this.installDate = installDate;
        this.status = status;
    }

    public Zlecenie()
    {
    }


}
