package pl.edu.vistula.annarapacz.restcl;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity
{
    public static DataBaseHelper db;
    public static String uri, apiKey;
    public static String hostPref, hostPort, uqKey;

    private ListView lv;
    private ZleceniaAdapter zleceniaAdapter;
    private List<Zlecenie> lstZlecenia;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        db = new DataBaseHelper(this);
        Log.d("DataBase", "Initial size: " + String.valueOf(db.getRecCount()));

            apiKey = db.getKey();
            hostPref = db.getHost();
            hostPort = db.getPort();
            uqKey = db.getUQKey();

        lv = findViewById(R.id.lstViewZlecenia);
        lstZlecenia = new ArrayList<>();
        lstZlecenia = db.getRecords();

        zleceniaAdapter = new ZleceniaAdapter(getApplicationContext(), lstZlecenia);
        lv.setAdapter(zleceniaAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String z = String.valueOf(view.getTag());
                Intent intent = new Intent(view.getContext(), ZlecenieActivity.class);
                intent.putExtra("zlID", z);
                startActivity(intent);
            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                reloadData("MAIN");
            }
        });


        FloatingActionButton fabSettings = (FloatingActionButton) findViewById(R.id.fabSettings);
        fabSettings.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });


        uri = hostPref + ":" + hostPort;


    }




    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void reloadData(String tag)
    {
        Log.d(tag, "reloading data...");
        uri = hostPref + ":" + hostPort;
        new RESTTask().execute(uri);
    }

    class RESTTask extends AsyncTask<String, Void, ResponseEntity<Zlecenie[]>>
    {
        protected ResponseEntity<Zlecenie[]> doInBackground(String... uri)
        {
            final String url = uri[0] + "/api/API/" + uqKey + "/";
            Log.d("REST", "url=" + url);
            RestTemplate restTemplate = new RestTemplate();

            ResponseEntity<Zlecenie[]> response;
            try
            {
                MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
                mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
                restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

                HttpHeaders headers = new HttpHeaders();
                headers.set("API_KEY", apiKey);
                HttpEntity<String> entity = new HttpEntity<String>(headers);

                response = restTemplate.exchange(url, HttpMethod.GET, entity, Zlecenie[].class);

                Log.d("RESTTASK", response.toString());
//                Log.d("RESTTASK", response.getBody().toString());
            }
            catch (Exception ex)
            {
                String message = ex.getMessage();
                Log.d("REST", message);
                response= null;
            }
            return response;
        }

        protected void onPostExecute(ResponseEntity<Zlecenie[]> result)
        {
            if (result == null)
            {
                Log.d("REST", "result is null");
                return;
            }
            HttpStatus statusCode = result.getStatusCode();

            Zlecenie[] zlecenia = result.getBody();
            if (!result.hasBody()) return;

            lstZlecenia.clear();
            if (zlecenia.length == 0 || zlecenia == null)
            {
                Log.d("REST", "nie sparsowałem zlecenia[]");
            }
            db.DeleteAllRecords();
            for (int i=0; i< zlecenia.length; i++)
            {
                db.AddEntry(zlecenia[i]);
                lstZlecenia.add(zlecenia[i]);
            }

            zleceniaAdapter.notifyDataSetChanged();

        }
    }
}
