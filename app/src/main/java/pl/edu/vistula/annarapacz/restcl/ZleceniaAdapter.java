package pl.edu.vistula.annarapacz.restcl;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by arapa on 2017-12-29.
 */

public class ZleceniaAdapter extends BaseAdapter
{
    private Context context;
    private List<Zlecenie> lstZlecenia;

    public ZleceniaAdapter(Context context, List<Zlecenie> lstZlecenia)
    {
        this.context = context;
        this.lstZlecenia = lstZlecenia;
    }

    @Override
    public int getCount()
    {
        return lstZlecenia.size();
    }

    @Override
    public Object getItem(int i)
    {
        return lstZlecenia.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        View v = View.inflate(context, R.layout.item_zlecenie, null);

        TextView tZlecenie = (TextView) v.findViewById(R.id.txtZlecenia);
        TextView tUlica = (TextView) v.findViewById(R.id.txtUlica);
        TextView tMiasto = (TextView) v.findViewById(R.id.txtMiasto);
        TextView tTel = (TextView) v.findViewById(R.id.txtTelefon);
        TextView tTel2 = (TextView) v.findViewById(R.id.txtTelefon2);
        TextView tTerm = (TextView) v.findViewById(R.id.txtTerminal);
        TextView tData = (TextView) v.findViewById(R.id.txtData);
        TextView tStatus = (TextView) v.findViewById(R.id.txtStatus);


        tZlecenie.setText("ZLECENIE: " + lstZlecenia.get(i).getName());
        tUlica.setText(lstZlecenia.get(i).getStreet() + " \\ " + lstZlecenia.get(i).getStreet2());
        tMiasto.setText(lstZlecenia.get(i).getCity());


        tTerm.setText(lstZlecenia.get(i).getTerminalModel());

        String status="";
        switch (lstZlecenia.get(i).getStatus())
        {
            case "N":
                status = "NOWE";
                break;
            case "p":
                status = "PRZETWARZANE";
                break;
            case "f":
                status = "ZAKOŃCZONE";
                break;
            case "a":
                status = "NIEUDANE";
                break;
            default: // "u"
                status = "INNE";
                break;
        }
        tStatus.setText(status);

       v.setTag(String.valueOf(lstZlecenia.get(i).getId()));
 //           v.setTag((lstZlecenia.get(i)));
        return v;
    }
}
