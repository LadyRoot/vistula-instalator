package pl.edu.vistula.annarapacz.restcl;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;

import static pl.edu.vistula.annarapacz.restcl.MainActivity.apiKey;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.db;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.uqKey;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.uri;

public class ZlecenieActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    TextView tNazwa, tUlica, tMiasto, tTel, tTel2, tEmail, tTerm, tData, tInfo;

    Spinner spStatus;
    long zlId;
    Zlecenie zlecenie, originalZlecenie;
    Button btnSync;
    private final String tag = "ZLECENIE"; // do Loggera

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zlecenie);
        btnSync = (Button) findViewById(R.id.btnSync);

        tNazwa = findViewById(R.id.txtZlecenia);
        tUlica = findViewById(R.id.txtUlica);
        tMiasto = findViewById(R.id.txtMiasto);
        tTel = findViewById(R.id.txtTelefon);
        tTel2 = findViewById(R.id.txtTelefon2);
        tEmail = findViewById(R.id.txtEmail);

        tTerm = findViewById(R.id.txtTerminal);
        tData = findViewById(R.id.txtData);
        spStatus = findViewById(R.id.spnStatus);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.strArrStatus, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(adapter);
        spStatus.setOnItemSelectedListener(this);

        tInfo = (TextView) findViewById(R.id.edtInfo);


        zlId = 0;
        Bundle extras = getIntent().getExtras();
        String strId = extras.getString("zlID");

        if (strId.isEmpty() || strId == null)
        {
            Toast.makeText(this, "Nie mogę otworzyc zlecenia", Toast.LENGTH_LONG);
        }
        else
        {
            zlId = Long.valueOf(strId);
        }

        try
        {
            db.getReadableDatabase();
            zlecenie = db.getRecord(zlId);
            originalZlecenie = zlecenie;
        }
        catch (Exception sqlEx)
        {
            Log.d("DataBase", sqlEx.getMessage());
            Toast.makeText(this, "Ups! Coś poszło nie tak!", Toast.LENGTH_LONG);
        }
        finally
        {
            db.close();
        }

        fillView(zlecenie);
        addListenerOnSyncButton();


    }


    private void fillView(Zlecenie zlecenie)
    {
        tNazwa.setText(zlecenie.getName());
        tUlica.setText("ul.: " + zlecenie.getStreet() + "\n" + zlecenie.getStreet2());
        tMiasto.setText(zlecenie.getCity());
        tTel.setText(zlecenie.getPhone());
        tTel2.setText(zlecenie.getPhone2());
        tEmail.setText(zlecenie.getEmail());
        tTerm.setText(zlecenie.getTerminalTID() + "\n" + zlecenie.getTerminalModel() + "\n" + zlecenie.getTerminalSerial() + "\n" + zlecenie.getTerminalDescription());
        tData.setText(zlecenie.getInstallDate());

        switch (zlecenie.getStatus())
        {
            case "N":
                spStatus.setSelection(0);
                break;
            case "p":
                spStatus.setSelection(1);
                break;
            case "f":
                spStatus.setSelection(2);
            break;
            case "a":
                spStatus.setSelection(3);
                break;
            default: // "u"
                spStatus.setSelection(4);
                break;
        }

        if (zlecenie.getInfo() != "") tInfo.setText(zlecenie.getInfo());

        getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
    }


    private void addListenerOnSyncButton()
    {
        btnSync.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    db.getWritableDatabase();

                    // compare
                    if ((zlecenie.getStatus() != originalZlecenie.getStatus()) || (tInfo.getText() != originalZlecenie.getInfo()))
                    {
                        zlecenie.setInfo(tInfo.getText().toString());
                        // save db
                        db.UpdateRecord(zlecenie);
                        // POST
                        new RESTPost().execute(uri);
                    }
                    else
                    {
                        refresh();
                    }
                    // do nothing to save locally

                }
                catch (android.database.SQLException ex)
                {
                    Log.d(tag, ex.getLocalizedMessage());
                }
                catch (Exception ex)
                {
                    Log.d(tag, ex.getMessage());
                }

                //new RESTGet().execute(uri);
            }
        });
        // save data to database

    }

    private void refresh()
    {
        new RESTGet().execute(uri);
    }

    private void update()
    {
//        new RESTPost().execute(uri);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        //String item = adapterView.getItemAtPosition(i).toString();
        String status;
        switch (i)
        {
            case 0: // NOWE
                status = "N";
                break;
            case 1: // W TRAKCIE PRZETWARZANIA
                status = "p";
                break;
            case 2: // ZAKOŃCZONE - SUKCES
                status = "f";
                break;
            case 3: // ZAKOŃCZONE - NIEUDANE
                status = "a";
                break;
            case 4: // STATUS: INNY
                status = "u";
                break;
            default:
                status = "u";
                break;
        }
        zlecenie.setStatus(status);

        try
        {
            db.getWritableDatabase();
            db.UpdateRecord(zlecenie);
        }
        catch (Exception ex)
        {
            Log.d(tag, ex.getLocalizedMessage());
        }
        finally
        {
            db.close();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {
        // Zostaw tak, jak jest - wartość jest już przypisana
    }


    class RESTGet extends AsyncTask<String, Void, ResponseEntity<Zlecenie>>
    {
        protected ResponseEntity<Zlecenie> doInBackground(String... uri)
        {
            final String url = uri[0] + "/api/API/" + uqKey + "?id2=" + String.valueOf(zlId);
            Log.d("REST", "url=" + url);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Zlecenie> response;
            try
            {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpHeaders headers = new HttpHeaders();
                headers.set("API_KEY", apiKey);

                HttpEntity<String> entity = new HttpEntity<String>(headers);

                response = restTemplate.exchange(url, HttpMethod.GET, entity, Zlecenie.class);
            }
            catch (Exception ex)
            {
                String message = ex.getMessage();
                Log.d("REST", message);
                response = null;
            }
            return response;
        }

        protected void onPostExecute(ResponseEntity<Zlecenie> result)
        {
            if (result == null)
            {
                Log.d("REST", "result is null");
                return;
            }
            HttpStatus statusCode = result.getStatusCode();

            Zlecenie otrzymane = result.getBody();
            if (otrzymane == null)
            {
                return;
            }
            else
            {
                db.UpdateRecord(otrzymane);
                zlecenie = otrzymane;
                fillView(zlecenie);
            }
        }

    }

    class RESTPost extends AsyncTask<String, Void, ResponseEntity<Boolean>>
    {
        protected ResponseEntity<Boolean> doInBackground(String... uri)
        {
            if (zlecenie.getInfo() == null || zlecenie.getInfo() == "")
            {
                zlecenie.setInfo("-");
            }
            String url = uri[0] + "/api/API/" + uqKey + "/" + String.valueOf(zlId) + "/" + zlecenie.getStatus() + "/" + zlecenie.getInfo();

            Log.d("REST", "url=" + url);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Boolean> response;
            try
            {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpHeaders headers = new HttpHeaders();
                headers.set("API_KEY", apiKey);

                HttpEntity<String> entity = new HttpEntity<String>(headers);


                response = restTemplate.exchange(url, HttpMethod.POST, entity, Boolean.class);

            }
            catch (Exception ex)
            {
                String message = ex.getMessage();
                Log.d("REST", message);
                response = null;
            }
            return response;
        }

        protected void onPostExecute(ResponseEntity<Boolean> result)
        {
            if (result == null)
            {
                Log.d("REST", "result is null");
                return;
            }
            HttpStatus statusCode = result.getStatusCode();

            //Zlecenie zlecenieReturned = result.getBody();
            if (result.getBody() == true)
            {
                Toast.makeText(ZlecenieActivity.this, "Zapisano poprawnie", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(ZlecenieActivity.this, "Synchronizacja z serwerem nie powiodła się.", Toast.LENGTH_SHORT).show();
            }
        }

    }
}