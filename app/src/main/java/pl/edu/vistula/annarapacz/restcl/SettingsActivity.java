package pl.edu.vistula.annarapacz.restcl;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static pl.edu.vistula.annarapacz.restcl.MainActivity.db;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.hostPort;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.hostPref;
import static pl.edu.vistula.annarapacz.restcl.MainActivity.uqKey;

public class SettingsActivity extends AppCompatActivity
{
    TextView txtHost, txtPort, txtKey;
    Button b;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        txtHost = (TextView) findViewById(R.id.txtSerwer);
        txtPort = (TextView) findViewById(R.id.txtPort);
        txtKey = (TextView) findViewById(R.id.txtKey);
        b = (Button) findViewById(R.id.btnSaveSettings);

            txtHost.setText(hostPref);
            txtPort.setText(hostPort);
            txtKey.setText(uqKey);


        b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                db.getWritableDatabase();
                    db.UpdatePort(txtPort.getText().toString());
                    db.UpdateHost(txtHost.getText().toString());
                    db.UpdateUQKey(txtKey.getText().toString());
                db.close();
                hostPort = txtPort.getText().toString();
                hostPref = txtHost.getText().toString();
                uqKey = txtKey.getText().toString();

                Intent i= new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(i);
            }



        });

    }


}
