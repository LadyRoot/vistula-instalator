package pl.edu.vistula.annarapacz.restcl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class DataBaseHelper extends SQLiteOpenHelper
{
    private static final int DB_VERSION = 23;
    private static final String ZLECENIATB = "ZLECENIA";

    DataBaseHelper(Context context)
    {
        super (context, "Instalacje_terminali.db", null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // jeśli nie ma jeszcze nic, to tworzymy sobie obiekty, czyli jedna tabelka
        db.execSQL(
                "CREATE TABLE " + ZLECENIATB + " ("+
                        "zl_id LONG PRIMARY KEY," +
                        "zl_info NVARCHAR," +
                        "zl_name NVARCHAR," +
                        "zl_street NVARCHAR," +
                        "zl_street2 NVARCHAR," +
                        "zl_city NVARCHAR,"+
                        "zl_phone NVARCHAR,"+
                        "zl_phone2 NVARCHAR,"+
                        "zl_email NVARCHAR,"+
                        "zl_termTID NVARCHAR," +
                        "zl_termSerial NVARCHAR," +
                        "zl_termDesc NVARCHAR," +
                        "zl_termModel NVARCHAR," +
                        "zl_simIMEI NVARCHAR," +
                        "zl_simDESC NVARCHAR," +
                        "zl_dte NVARCHAR," +
                        "zl_status NVARCHAR);"
        );
        db.execSQL( "CREATE TABLE APPKEY ( APIKEY NVARCHAR );" );
        db.execSQL( "INSERT INTO APPKEY VALUES ( 'X-some-key' );" );

        db.execSQL(
                "CREATE TABLE PREFERENCESTB ("+
                        "PREFKEY NVARCHAR PRIMARY KEY," +
                        "PREFVALUE NVARCHAR);");
        db.execSQL( "INSERT INTO PREFERENCESTB (PREFKEY, PREFVALUE) VALUES ( 'HostAddress','http://insterm.azurewebsites.net' );" );
        db.execSQL( "INSERT INTO PREFERENCESTB (PREFKEY, PREFVALUE) VALUES ( 'HostPort','80' );" );
        db.execSQL( "INSERT INTO PREFERENCESTB (PREFKEY, PREFVALUE) VALUES ( 'UQKey','0' );" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL ( "DROP TABLE PREFERENCESTB;");
        db.execSQL ( "DROP TABLE " + ZLECENIATB + ";" );
        db.execSQL ( "CREATE TABLE IF NOT EXISTS APPKEY ( APIKEY NVARCHAR );" );
        db.execSQL (
                "CREATE TABLE " + ZLECENIATB + " ("+
                        "zl_id LONG PRIMARY KEY," +
                        "zl_info NVARCHAR," +
                        "zl_name NVARCHAR," +
                        "zl_street NVARCHAR," +
                        "zl_street2 NVARCHAR," +
                        "zl_city NVARCHAR,"+
                        "zl_phone NVARCHAR,"+
                        "zl_phone2 NVARCHAR,"+
                        "zl_email NVARCHAR,"+
                        "zl_termTID NVARCHAR," +
                        "zl_termSerial NVARCHAR," +
                        "zl_termDesc NVARCHAR," +
                        "zl_termModel NVARCHAR," +
                        "zl_simIMEI NVARCHAR," +
                        "zl_simDESC NVARCHAR," +
                        "zl_dte NVARCHAR," +
                        "zl_status NVARCHAR);"
        );
        db.execSQL( "DELETE FROM APPKEY;" ); // if exists
        db.execSQL( "INSERT INTO APPKEY VALUES ( 'X-some-key' );" );

        db.execSQL(
                "CREATE TABLE PREFERENCESTB ("+
                        "PREFKEY NVARCHAR PRIMARY KEY," +
                        "PREFVALUE NVARCHAR);");
        db.execSQL( "INSERT INTO PREFERENCESTB (PREFKEY, PREFVALUE) VALUES ( 'HostAddress','http://insterm.azurewebsites.net' );" );
        db.execSQL( "INSERT INTO PREFERENCESTB (PREFKEY, PREFVALUE) VALUES ( 'HostPort','80' );" );
        db.execSQL( "INSERT INTO PREFERENCESTB VALUES ( 'UQKey','0' );" );
    }



    //====================================================
    // CRUD
    // będziemy dodawać/modyfikować/usuwać rekordy z bazy
    //====================================================

    public void AddEntry(Zlecenie record)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("zl_id", record.getId());
        values.put("zl_info", record.getInfo());
        values.put("zl_name", record.getName());
        values.put("zl_street", record.getStreet());
        values.put("zl_street2", record.getStreet2());
        values.put("zl_city", record.getCity());
        values.put("zl_phone", record.getPhone());
        values.put("zl_phone2", record.getPhone2());
        values.put("zl_email", record.getEmail());
        values.put("zl_termTID", record.getTerminalTID());
        values.put("zl_termSerial", record.getTerminalSerial());
        values.put("zl_termDesc", record.getTerminalDescription());
        values.put("zl_termModel", record.getTerminalModel());
        values.put("zl_simIMEI", record.getSimImei());
        values.put("zl_simDESC", record.getSimDesc());
        values.put("zl_dte", record.getInstallDate());
        values.put("zl_status", record.getStatus());

        try
        {
            db.insert(ZLECENIATB, null, values);
        }
        catch (Exception ex)
        {
            Log.d("DataBase",ex.getMessage());
        }
        finally
        {
            db.close();
        }

    }

    public String getKey()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String _query = "SELECT APIKEY FROM APPKEY;";
        String key="";
        Cursor cursor = db.rawQuery(_query, null);
        if (cursor.getCount()>0) // cuś jest
        {
            cursor.moveToFirst();
            key =  cursor.getString(0);
        }
        cursor.close();
        db.close();
        Log.d("SQL", "getKey " + key);
        return key;
    }
    public String getHost()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String _query = "SELECT PREFVALUE FROM PREFERENCESTB WHERE PREFKEY='HostAddress';";
        String key="";
        Cursor cursor = db.rawQuery(_query, null);
        if (cursor.getCount()>0) // cuś jest
        {
            cursor.moveToFirst();
            key =  cursor.getString(0);
        }
        cursor.close();
        db.close();
        Log.d("SQL", "getHost " + key);
        return key;
    }
    public String getPort()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String _query = "SELECT PREFVALUE FROM PREFERENCESTB WHERE PREFKEY='HostPort';";
        String key="";
        Cursor cursor = db.rawQuery(_query, null);
        if (cursor.getCount()>0) // cuś jest
        {
            cursor.moveToFirst();
            key =  cursor.getString(0);
        }
        cursor.close();
        db.close();
        Log.d("SQL", "getPort " + key);
        return key;
    }
    public String getUQKey()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String _query = "SELECT PREFVALUE FROM PREFERENCESTB WHERE PREFKEY='UQKey';";
        String key="";
        Cursor cursor = db.rawQuery(_query, null);
        if (cursor.getCount()>0) // cuś jest
        {
            cursor.moveToFirst();
            key =  cursor.getString(0);
        }
        cursor.close();
        db.close();
        Log.d("SQL", "getUQKey " + key);
        return key;
    }


    public void UpdateHost(String newAddress)
    {
        String query = "UPDATE PREFERENCESTB SET PREFVALUE='" + newAddress + "' WHERE PREFKEY='HostAddress';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }
    public void UpdatePort(String newPort)
    {
        String query = "UPDATE PREFERENCESTB SET PREFVALUE='" + newPort + "' WHERE PREFKEY='HostPort';";
        SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
        db.close();
    }
    public void UpdateUQKey(String newKey)
    {
        String query = "UPDATE PREFERENCESTB SET PREFVALUE='" + newKey + "' WHERE PREFKEY='UQKey';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }


    public Zlecenie getRecord(long id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.query(ZLECENIATB, new String[]{"zl_info", "zl_name","zl_street","zl_street2","zl_city",
                            "zl_phone","zl_phone2", "zl_email",
                            "zl_termTID", "zl_termSerial", "zl_termModel","zl_termDesc", "zl_simIMEI", "zl_simDESC",
                            "zl_dte", "zl_status"},
                    "zl_id=?", new String[] {String.valueOf(id)}, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            Zlecenie record = new Zlecenie(id,
                    cursor.getString(0), // zl_info
                    cursor.getString(1), // zl_name
                    cursor.getString(2), // zl_street
                    cursor.getString(3), // zl_street2
                    cursor.getString(4), // zl_city
                    cursor.getString(5), // zl_phone
                    cursor.getString(6), // zl_phone2
                    cursor.getString(7), // zl_email
                    cursor.getString(8), // zl_termTID
                    cursor.getString(9), // zl_termSerial
                    cursor.getString(10), // zl_termModel
                     cursor.getString(11), // zl_termDesc
                    cursor.getString(12), // zl_simIMEI
                    cursor.getString(13), // zl_simDESC
                    cursor.getString(14), // zl_dte
                    cursor.getString(15) // zl_status
            );
            cursor.close();
            db.close();
            return record;
        }
        catch (Exception ex)
        {
            Log.d("DataBase", ex.getMessage());
            db.close();
            return null;
        }
    }

    public List<Zlecenie> getRecords()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String _query = "SELECT zl_id,zl_info,zl_name,zl_street,zl_street2,zl_city,zl_phone,zl_phone2,zl_email,zl_termTID,zl_termSerial,zl_termModel,zl_termDesc,zl_simIMEI,zl_simDESC, zl_dte,zl_status FROM " + ZLECENIATB + " ORDER BY zl_id;";
        Cursor cursor = db.rawQuery(_query, null);
        List<Zlecenie> records = new ArrayList<Zlecenie>();

        // loop po każdym rekordzie i dodajemy do bazy
        if (cursor.getCount()>0) // cuś jest
        {
            cursor.moveToFirst();
            do
            {
                Zlecenie r = new Zlecenie();
                r.setId((long) cursor.getInt(0));       // zl_id
                r.setInfo(cursor.getString(1));         // zl_info
                r.setName(cursor.getString(2));         // zl_name
                r.setStreet(cursor.getString(3));       // zl_street
                r.setStreet2(cursor.getString(4));      // zl_street2
                r.setCity(cursor.getString(5));         // zl_city
                r.setPhone(cursor.getString(6));        // zl_phone
                r.setPhone2(cursor.getString(7));       // zl_phone2
                r.setEmail(cursor.getString(8));        // zl_email
                r.setTerminalTID(cursor.getString(9));          // zl_termTID
                r.setTerminalSerial(cursor.getString(10));      // zl_termSerial
                r.setTerminalModel(cursor.getString(11));       // zl_termModel
                r.setTerminalDescription(cursor.getString(12)); // zl_termDesc
                r.setSimImei(cursor.getString(13));     // zl_simIMEI
                r.setSimDesc(cursor.getString(14));     // zl_simDESC
                r.setInstallDate(cursor.getString(15)); // zl_dte
                r.setStatus(cursor.getString(16));      // zl_status
                records.add(r);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return records;
    }

    public long getRecCount()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        long cnt = DatabaseUtils.queryNumEntries(db, ZLECENIATB);
        db.close();
        return cnt;
    }

    public int UpdateRecord (Zlecenie record)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("zl_id", record.getId());
        values.put("zl_info", record.getInfo());
        values.put("zl_name", record.getName());
        values.put("zl_street", record.getStreet());
        values.put("zl_street2", record.getStreet2());
        values.put("zl_city", record.getCity());
        values.put("zl_phone", record.getPhone());
        values.put("zl_phone2", record.getPhone2());
        values.put("zl_email", record.getEmail());
        values.put("zl_termTID", record.getTerminalTID());
        values.put("zl_termSerial", record.getTerminalSerial());
        values.put("zl_termDesc", record.getTerminalDescription());
        values.put("zl_termModel", record.getTerminalModel());
        values.put("zl_simIMEI", record.getSimImei());
        values.put("zl_simDESC", record.getSimDesc());
        values.put("zl_dte", record.getInstallDate());
        values.put("zl_status", record.getStatus());

        int i = db.update(ZLECENIATB, values, "zl_id=?", new String[] {String.valueOf(record.getId())});

        db.close();
        return i; // to jest rezultat operacji
    }


    // tu już po linii najmniejszego oporu :)
    public void DeleteRecord(Zlecenie record)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ZLECENIATB, "zl_id=?", new String[]{String.valueOf(record.getId())});
        db.close();
    }

    public void DeleteAllRecords()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(
                "DELETE FROM " + ZLECENIATB + " ;"
        );
        db.close();
    }
//
}
